package com.example.tiendavirtual;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class activity_registrar extends AppCompatActivity {

    private EditText txtEmail;
    private EditText txtUsuario;
    private EditText txtPassword;
    private EditText txtPassword2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
    }

    public void irAtras(View view){
        Intent regresar = new Intent(this,MainActivity.class);
        startActivity(regresar);
    }

    public  void registrarUsuarios(View view){
        AdminSql admin= new AdminSql(this,"usuarios",null,1);
        SQLiteDatabase basedatos=admin.getReadableDatabase();

        String usuario=txtUsuario.getText().toString();
        String email=txtEmail.getText().toString();
        String password=txtPassword.getText().toString();

        if(!usuario.isEmpty() && !email.isEmpty() && !password.isEmpty()){

            ContentValues registro = new ContentValues();

            registro.put("usuario",usuario);
            registro.put("email",email);
            registro.put("password",password);

            basedatos.insert("usuarios",null,registro);

            basedatos.close();

            txtEmail.setText("");
            txtPassword.setText("");
            txtUsuario.setText("");
            txtPassword2.setText("");
            Toast.makeText(this, "EL usuario se guardo exitosamente", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "Los campos no pueden estar vacios", Toast.LENGTH_LONG).show();
        }
    }
}