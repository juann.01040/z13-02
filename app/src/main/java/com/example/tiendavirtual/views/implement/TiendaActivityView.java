package com.example.tiendavirtual.views.implement;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.tiendavirtual.R;
import com.example.tiendavirtual.views.interfaces.ITiendaView;

public class TiendaActivityView extends AppCompatActivity implements ITiendaView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tienda_view);
    }
}